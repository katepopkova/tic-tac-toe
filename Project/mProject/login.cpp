#include "login.h"
#include "ui_login.h"

LogIn::LogIn(QWidget *parent) : QWidget(parent), ui(new Ui::LogIn)
{
    ui->setupUi(this);

    ui->pbEnter->setDefault(true);
    ui->pbEnter->setEnabled(false);

    message=new QSound("C:/Users/Ekaterina/Documents/scifi1.wav");

    QRegExpValidator *name1Validator=new QRegExpValidator(QRegExp("[a-zA-Z0-9]{20}"), this);
    QRegExpValidator *pword1Validator=new QRegExpValidator(QRegExp("[a-zA-Z0-9]{10}"), this);
    QRegExpValidator *name2Validator=new QRegExpValidator(QRegExp("[a-zA-Z0-9]{20}"), this);
    QRegExpValidator *pword2Validator=new QRegExpValidator(QRegExp("[a-zA-Z0-9]{10}"), this);

    ui->leName1->setValidator(name1Validator);
    ui->lePassword1->setValidator(pword1Validator);
    ui->leName3->setValidator(name2Validator);
    ui->lePassword3->setValidator(pword2Validator);

    connect(ui-> leName1, SIGNAL(textChanged(QString)), this, SLOT(logInFilled()));
    connect(ui-> lePassword1, SIGNAL(textChanged(QString)), this, SLOT(logInFilled()));
    connect(ui-> leName3, SIGNAL(textChanged(QString)), this, SLOT(logInFilled()));
    connect(ui-> lePassword3, SIGNAL(textChanged(QString)), this, SLOT(logInFilled()));

    mainWindow = new mainwindow(this);
    registrationWindow = new CheckIn(this);
    socket = new QTcpSocket(this);

    connect(ui->pbEnter, SIGNAL(clicked()), this, SLOT(pbEnterClicked()));
    connect(ui->pbCheckIn1, SIGNAL(clicked()), this, SLOT(pbCheckIn1Clicked()));
    connect(ui->pbExit, SIGNAL(clicked()), this, SLOT(close()));
    connect(this, SIGNAL(sendUser1(QString)), mainWindow, SLOT(recieveUser1(QString)));
    connect(this, SIGNAL(sendUser2(QString)), mainWindow, SLOT(recieveUser2(QString)));
}

LogIn::~LogIn()
{
    delete ui;
}

void LogIn::pbEnterClicked()
{
    QString out="login:"+ui->leName1->text()+":"+ui->lePassword1->text()+":"+ui->leName3->text()+":"+ui->lePassword3->text()+":";

    socket->connectToHost(QHostAddress::LocalHost, 4545);

    if(socket->waitForConnected(5000))
    {
        qDebug() << "Connected!";
        socket->write(out.toLatin1());
        socket->waitForBytesWritten(1000);
        socket->waitForReadyRead(3000);

        QString cmdFrServ=socket->readAll();
        qDebug() << cmdFrServ;
        if(cmdFrServ==QString("fnopened"))
        {
            message->play();
            QMessageBox::information(this, "Ошибка!", "Файл 'users.txt' не найден!");
            return;
        }
        else if(cmdFrServ==QString("loginok"))
        {            
            emit sendUser1(ui->leName1->text()); //сигнал, куда-то передающий введенные данные
            emit sendUser2(ui->leName3->text());
            ui->leName1->clear();
            ui->lePassword1->clear();
            ui->leName3->clear();
            ui->lePassword3->clear();
            this->hide();
            mainWindow->show();
            return;
        }
        else if(cmdFrServ==QString("user1error"))
        {
            QString text="Игрок "+QString::number(1)+": имя пользователя или пароль некорректны!";
            message->play();
            QMessageBox::information(this, "Внимание!", text);
            ui->leName1->clear();
            ui->lePassword1->clear();
        }
        else if(cmdFrServ==QString("user2error"))
        {
            QString text="Игрок "+QString::number(2)+": имя пользователя или пароль некорректны!";
            message->play();
            QMessageBox::information(this, "Внимание!", text);
            ui->leName3->clear();
            ui->lePassword3->clear();
        }
        else
        {
            message->play();
            QMessageBox::information(this, "Внимание!", "Имена или пароли пользователей не корректны");
            ui->leName1->clear();
            ui->lePassword1->clear();
            ui->leName3->clear();
            ui->lePassword3->clear();
        }
    }
    else
    {
        qDebug() << "Not connected!";
    }
    return;
}

void LogIn::pbCheckIn1Clicked()
{
    ui->leName1->clear();
    ui->lePassword1->clear();
    this->hide();
    registrationWindow->show();
}

void LogIn::logInFilled()
{
    if(ui->leName1->text()==QString("") && ui->lePassword1->text()==QString("") && ui->leName3->text()==QString(""))
        ui->pbEnter->setEnabled(false);
    else
        ui->pbEnter->setEnabled(!ui->lePassword3->text().isEmpty());
}
