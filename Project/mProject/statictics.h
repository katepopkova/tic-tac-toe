#ifndef STATICTICS_H
#define STATICTICS_H

#include <QWidget>
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QtMultimedia/QSound>
#include <QTcpSocket>
#include <QHostAddress>
#include <QStringList>
#include <QDebug>

namespace Ui {
class Statictics;
}

class mainwindow;

class Statictics : public QWidget
{
    Q_OBJECT

public:
    explicit Statictics(mainwindow*, QWidget *parent = 0);
    ~Statictics();

private:
    void tableSettings();

    Ui::Statictics *ui;
    mainwindow *ch;
    QTableWidget *tableWidget;
    QTableWidgetItem *itemUser1;
    QTableWidgetItem *itemUser2;
    QTableWidgetItem *itemScore;
    QSound *message;
    QTcpSocket *socket;

private slots:
    void pbBackClicked();
    void pbOutputStatClicked();
    void recieveUsers(QString users);
};

#endif // STATICTICS_H
