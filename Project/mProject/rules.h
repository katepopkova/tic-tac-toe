#ifndef RULES_H
#define RULES_H

#include <QWidget>
#include <QTextBrowser>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QtMultimedia/QSound>
#include "mainwindow.h"
#include "ui_mainwindow.h"

namespace Ui {
class Rules;
}

class mainwindow;

class Rules : public QWidget
{
    Q_OBJECT

public:
    explicit Rules(mainwindow*, QWidget *parent = 0);
    ~Rules();

private:
    Ui::Rules *ui;
    mainwindow *mwin;
    QTextBrowser *textBrowser;
    QSound *message;
    QTcpSocket *socket;

private slots:
    void pbBack2Clicked();
};

#endif // RULES_H
