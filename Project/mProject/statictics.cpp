#include "statictics.h"
#include "ui_statictics.h"

Statictics::Statictics(mainwindow *c, QWidget *parent) : QWidget(parent), ui(new Ui::Statictics)
{
    ui->setupUi(this);
    ch=c;
    message=new QSound("C:/Users/Ekaterina/Documents/scifi1.wav");
    socket = new QTcpSocket(this);

    ui->pbOutputStat->setDefault(true);
    ui->tableWidget->setRowCount(20);
    ui->tableWidget->setColumnCount(3);
    ui->tableWidget->setEditTriggers(0);
    tableSettings();
    message=new QSound("C:/Users/Ekaterina/Documents/scifi1.wav");

    connect(ui->pbBack, SIGNAL(clicked()), this, SLOT(pbBackClicked()));
    connect(ui->pbOutputStat, SIGNAL(clicked()), this, SLOT(pbOutputStatClicked()));
}

Statictics::~Statictics()
{
    delete ui;
}

void Statictics::pbBackClicked()
{
    ui->tableWidget->clear();
    tableSettings();
    ui->pbOutputStat->setEnabled(true);
    this->hide();
    ch->show();
}

void Statictics::pbOutputStatClicked()
{
    ui->tableWidget->clear();
    tableSettings();

    QString user1="stat:"+ui->rbUser1->text()+":";
    QString user2="stat:"+ui->rbUser2->text()+":";

    if(!ui->rbUser1->isChecked() && !ui->rbUser2->isChecked())
     {
          message->play();
          QMessageBox::information(this, "Внимание!", "Выберите игрока для вывода статистики!");
     }

    socket->connectToHost(QHostAddress::LocalHost, 4545);

        if(socket->waitForConnected(5000))
        {
            qDebug() << "Connected!";
            if(ui->rbUser1->isChecked())
                  socket->write(user1.toLatin1());
            else if(ui->rbUser2->isChecked())
                  socket->write(user2.toLatin1());
            socket->waitForBytesWritten(1000);
            socket->waitForReadyRead(3000);

            QString cmdFrServ=socket->readAll();
            qDebug() << cmdFrServ;
            if(cmdFrServ==QString("fnopened"))
            {
                  message->play();
                  QMessageBox::information(this, "Ошибка!", "Файл 'statistics.txt' не найден!");
                  return;
            }
            else
            {
                QStringList lstStat=cmdFrServ.split("\n");
                QString strStat=lstStat.join("");
                QStringList lstStatistics=strStat.split(" ");
                int j=0;
                for (int i = 0; i < (lstStatistics.size()/3); ++i)
                {
                    ui->tableWidget->insertRow(i);
                    ui->tableWidget->setItem(i, 0, new QTableWidgetItem(lstStatistics.at(j)));
                    ui->tableWidget->setItem(i, 1, new QTableWidgetItem(lstStatistics.at(++j)));
                    ui->tableWidget->setItem(i, 2, new QTableWidgetItem(lstStatistics.at(++j)));
                    ++j;
                }
            }
        socket->close();
    }
    else
    {
        qDebug() << "Not connected!";
    }
}

void Statictics::recieveUsers(QString users)
{
    QStringList lstUsers=users.split(":");
    ui->rbUser1->setText(lstUsers.at(0));
    ui->rbUser2->setText(lstUsers.at(1));
}

void Statictics::tableSettings()
{
    ui->tableWidget->setHorizontalHeaderLabels(QStringList()<<"Игрок 1"<<"Игрок 2"<<"Счет");
    ui->tableWidget->horizontalHeader()->setSectionResizeMode(QHeaderView::Stretch);
    ui->tableWidget->verticalHeader()->setSectionResizeMode(QHeaderView::Stretch);
}



