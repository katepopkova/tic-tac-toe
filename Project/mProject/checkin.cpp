#include "checkin.h"
#include "ui_checkin.h"

CheckIn::CheckIn(LogIn *p, QWidget *parent) : QWidget(parent), ui(new Ui::CheckIn)
{
    ui->setupUi(this);
    lg=p;
    message=new QSound("C:/Users/Ekaterina/Documents/scifi1.wav");
    socket = new QTcpSocket(this);


    ui->pbCheckIn2->setDefault(true);
    ui->pbCheckIn2->setEnabled(false);

    ui->leName2->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z0-9]{20}"), this));
    ui->lePassword2->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z0-9]{10}"), this));
    ui->leRepeatPassword->setValidator(new QRegExpValidator(QRegExp("[a-zA-Z0-9]{10}"), this));

    connect(ui->leName2, SIGNAL(textChanged(QString)), this, SLOT(checkInFilled()));
    connect(ui->lePassword2, SIGNAL(textChanged(QString)), this, SLOT(checkInFilled()));
    connect(ui->leRepeatPassword, SIGNAL(textChanged(QString)), this, SLOT(checkInFilled()));

    connect(ui->pbCheckIn2, SIGNAL(clicked()), this, SLOT(pbCheckIn2Clicked()));
    connect(ui->pbCancel, SIGNAL(clicked()), this, SLOT(pbCancelClicked()));
}

CheckIn::~CheckIn()
{
    delete ui;
}

void CheckIn::pbCheckIn2Clicked()
{
    if(ui->lePassword2->text()!=ui->leRepeatPassword->text())
    {
        message->play();
        QMessageBox::information(this, "Внимание!", "Пароли не совпадают.");
        ui->lePassword2->clear();
        ui->leRepeatPassword->clear();
    }
    else
    {
        QString out="checkin:"+ui->leName2->text()+":"+ui->lePassword2->text()+":";

        socket->connectToHost(QHostAddress::LocalHost, 4545);

        if(socket->waitForConnected(5000))
        {
            qDebug() << "Connected!";
            socket->write(out.toLatin1());
            socket->waitForBytesWritten(1000);
            socket->waitForReadyRead(3000);

            QString cmdFrServ=socket->readAll();
            qDebug() << cmdFrServ;
            if(cmdFrServ==QString("fnopened"))
            {
                message->play();
                QMessageBox::information(this, "Ошибка!", "Файл 'users.txt' не найден!");
                return;
            }
            else if(cmdFrServ==QString("usexist"))
            {
                message->play();
                QMessageBox::information(this, "Внимание!", "Пользователь с таким именем уже существует.");
                ui->leName2->clear();
                ui->lePassword2->clear();
                ui->leRepeatPassword->clear();
                return;
            }

            socket->close();
        }
        else
        {
            qDebug() << "Not connected!";
        }

        ui->leName2->clear();
        ui->lePassword2->clear();
        ui->leRepeatPassword->clear();
        this->hide();
        lg->show();
    }
}

void CheckIn::pbCancelClicked()
{
    this->hide();
    lg->show();
}

void CheckIn::checkInFilled()
{
    if(ui->leName2->text()==QString("") && ui->lePassword2->text()==QString(""))
        ui->pbCheckIn2->setEnabled(false);
    else
        ui->pbCheckIn2->setEnabled(!ui->leRepeatPassword->text().isEmpty());
}

