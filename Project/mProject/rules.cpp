#include "rules.h"
#include "ui_rules.h"

Rules::Rules(mainwindow *m, QWidget *parent) : QWidget(parent), ui(new Ui::Rules)
{
    ui->setupUi(this);
    ui->pbBack2->setDefault(true);
    mwin=m;
    textBrowser=new QTextBrowser();
    message=new QSound("C:/Users/Ekaterina/Documents/scifi1.wav");

    connect(ui->pbBack2, SIGNAL(clicked()), this, SLOT(pbBack2Clicked()));

    socket = new QTcpSocket(this);

    socket->connectToHost(QHostAddress::LocalHost, 4545);

    if(socket->waitForConnected(5000))
    {
        qDebug() << "Connected!";
        socket->write("rules:");
        socket->waitForBytesWritten(1000);
        socket->waitForReadyRead(3000);

        QString cmdFrServ=socket->readAll();
        if(cmdFrServ==QString("fnopened"))
        {
            message->play();
            QMessageBox::information(this, "Ошибка!", "Файл 'rules.txt' не найден!");
            return;
        }
        else
            ui->textBrowser->setText(cmdFrServ);

        socket->close();
    }
    else
    {
        qDebug() << "Not connected!";
    }
}

Rules::~Rules()
{
    delete ui;
}

void Rules::pbBack2Clicked()
{
    this->hide();
    mwin->show();
}


