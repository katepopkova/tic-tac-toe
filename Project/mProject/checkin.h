#ifndef CHECKIN_H
#define CHECKIN_H

#include <QWidget>
#include "login.h"
#include "ui_login.h"
#include <QMessageBox>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QtMultimedia/QSound>

namespace Ui {
class CheckIn;
}

class LogIn;

class CheckIn : public QWidget
{
    Q_OBJECT

public:
    explicit CheckIn(LogIn*, QWidget *parent = 0);
    ~CheckIn();

private:
    Ui::CheckIn *ui;
    LogIn *lg;
    QSound *message;
    QTcpSocket *socket;

private slots:
    void checkInFilled();
    void pbCheckIn2Clicked();
    void pbCancelClicked();
};

#endif // CHECKIN_H
