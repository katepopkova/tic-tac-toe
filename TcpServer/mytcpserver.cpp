#include "mytcpserver.h"

MyTcpServer::MyTcpServer(QObject *parent) : QObject(parent)
{
    server=new QTcpServer(this);

    connect(server,SIGNAL(newConnection()),this,SLOT(newConnection()));
    if(!server->listen(QHostAddress::Any, 4545))
    {
        qDebug()<<"Error!";
    }
    else
    {
        qDebug()<<"Server started!";
    }
}

void MyTcpServer::newConnection()
{
     QFile usersFile("C:/Users/Ekaterina/Documents/users.txt");
     QFile statFile("C:/Users/Ekaterina/Documents/statistics.txt");
     QFile rulesFile("C:/Users/Ekaterina/Documents/rules.txt");

    socket = server->nextPendingConnection();
    socket->waitForReadyRead(3000);
    qDebug() << "Reading: " << socket->bytesAvailable();
    QString cmdFrClient=socket->readAll();
    qDebug() << cmdFrClient;

    QStringList lstCmdCl=cmdFrClient.split(":");

    //Регистрация
    if(lstCmdCl.at(0)==QString("checkin"))
    {
        bool exist=false;
        if(!usersFile.open(QIODevice::ReadOnly | QIODevice::Text))
            fileOpenError();
        while(!usersFile.atEnd())
        {
             QString strInFile=usersFile.readLine();
             QStringList lstUsers=strInFile.split(" ");
             if(lstCmdCl.at(1)==lstUsers.at(0))
             {
                QString string="usexist";
                writeMessageToClient(string);
                exist=true;
             }
        }
        usersFile.close();
        if(exist==false)
        {
        if(!usersFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
            fileOpenError();
        QTextStream streamUsers(&usersFile);
        streamUsers << lstCmdCl.at(1) << ' ' << lstCmdCl.at(2) << ' ' << '\n';
        usersFile.close();
        }
    }

    //Вывод правил
    if(lstCmdCl.at(0)==QString("rules"))
    {
        if(!rulesFile.open(QIODevice::ReadOnly | QIODevice::Text))
            fileOpenError();
        QString strInFileRules=rulesFile.readAll();
        socket->write(strInFileRules.toUtf8()); //для кодировки кириллицы
        socket->flush();
        socket->waitForBytesWritten(3000);
        socket->waitForReadyRead(3000);
        rulesFile.close();
    }

    //Авторизация
    if(lstCmdCl.at(0)==QString("login"))
    {
        bool user1=false;
        bool user2=false;
        if(!usersFile.open(QIODevice::ReadOnly | QIODevice::Text))
            fileOpenError();
        while(!usersFile.atEnd())
        {
             QString strInFile=usersFile.readLine();
             QStringList lst=strInFile.split(" ");
             if(lstCmdCl.at(1)==lst.at(0) && lstCmdCl.at(2)==lst.at(1))
                 user1=true;
        }
        usersFile.seek(0);
        while(!usersFile.atEnd())
        {
             QString strInFile=usersFile.readLine();
             QStringList lst=strInFile.split(" ");
             if(lstCmdCl.at(3)==lst.at(0) && lstCmdCl.at(4)==lst.at(1))
                 user2=true;
        }
        usersFile.close();

        if(user1==false && user2==true)
        {
            QString string="user1error";
            writeMessageToClient(string);
        }
        else if(user1==true && user2==false)
        {
            QString string="user2error";
            writeMessageToClient(string);
        }
        else if(user1==true && user2==true)
        {
            QString string="loginok";
            writeMessageToClient(string);
        }
       else
        {
            QString string="loginerror";
            writeMessageToClient(string);
        }
    }

    //Запись статистики
    if(lstCmdCl.at(0)==QString("sendstat"))
    {
        if(!statFile.open(QIODevice::WriteOnly | QIODevice::Append | QIODevice::Text))
            fileOpenError();
        QTextStream streamStat(&statFile);
        streamStat << lstCmdCl.at(1) << ' ' << lstCmdCl.at(2) << ' ' << lstCmdCl.at(3) << ':' << lstCmdCl.at(4) << ' ' << '\n';
        statFile.close();
    }

    //Вывод статистики
    if(lstCmdCl.at(0)==QString("stat"))
    {
        if(!statFile.open(QIODevice::ReadOnly | QIODevice::Text))
            fileOpenError();
        while(!statFile.atEnd())
        {
            QString strInFileStat=statFile.readLine();
            QStringList lstUsers=strInFileStat.split(" ");
            if(lstCmdCl.at(1)==lstUsers.at(0) || lstCmdCl.at(1)==lstUsers.at(1))
               {
                    data.append(strInFileStat);
                    lstUsers.clear();
               }
        }
        QString getStat=data.join("");
        data.clear();
        writeMessageToClient(getStat);
        statFile.close();
    }

    socket->close();
}

void MyTcpServer::fileOpenError()
{
    socket->write("fnopened");
    socket->flush();
    socket->waitForBytesWritten(3000);
    socket->waitForReadyRead(3000);
}

void MyTcpServer::writeMessageToClient(QString string)
{
    socket->write(string.toLatin1());
    socket->flush();
    socket->waitForBytesWritten(3000);
    socket->waitForReadyRead(3000);
}
