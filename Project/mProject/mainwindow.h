#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QWidget>
#include <QtGui>
#include <QMessageBox>
#include <QMouseEvent>
#include <QtMultimedia/QSound>
#include <QStringList>
#include <QTableWidget>
#include <QTableWidgetItem>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include "login.h"
#include "ui_login.h"
#include "statictics.h"
#include "ui_statictics.h"
#include "rules.h"
#include "ui_rules.h"

namespace Ui {
class mainwindow;
class QRect;
}

class LogIn;
class Statictics;
class Rules;

class mainwindow : public QWidget
{
    Q_OBJECT

public:
    explicit mainwindow(LogIn*, QWidget *parent = 0);
    ~mainwindow();

protected:
    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

private:
    int check();
    void sendStatistics(QString user1, QString user2, int scoreUser1, int scoreUser2);

    Ui::mainwindow *ui;
    LogIn *lg;
    Statictics *stat;
    Rules *rules;
    QSound *click;
    QSound *message;
    QTableWidget *tableWidget;
    QTcpSocket *socket;
    static const int rowCount=9;
    static const int colCount=9;
    static const int x0=25;
    static const int cellSize=70;
    QRect rects[rowCount * colCount];
    int arr[rowCount][colCount];
    int moves;

signals:
    void sendUsers(QString users);

private slots:
    void pbChangePlayerClicked();
    void pbStatisticsClicked();
    void pbRulesClicked();
    void pbNewGameClicked();
    void recieveUser1(QString user);
    void recieveUser2(QString user);
};

#endif // MAINWINDOW_H
