#ifndef MYTCPSERVER_H
#define MYTCPSERVER_H

#include <QObject>
#include <QTcpServer>
#include <QTcpSocket>
#include <QFile>
#include <QTextStream>
#include <QIODevice>
#include <QStringList>
#include <QString>

class MyTcpServer : public QObject
{
    Q_OBJECT

public:
    explicit MyTcpServer(QObject *parent = 0);
    void fileOpenError();
    void writeMessageToClient(QString string);

private:
    QTcpServer *server;
    QStringList data;
    QTcpSocket *socket;

public slots:
    void newConnection();
};

#endif // MYTCPSERVER_H
