#include "mainwindow.h"
#include "ui_mainwindow.h"

mainwindow::mainwindow(LogIn *p, QWidget *parent) : QWidget(parent), ui(new Ui::mainwindow)
{
    int num=0, rCount, cCount;

    ui->setupUi(this);
    lg=p;
    ui->pbNewGame->setDefault(true);

    stat=new Statictics(this);
    rules=new Rules(this);
    click=new QSound("C:/Users/Ekaterina/Documents/09.wav");
    message=new QSound("C:/Users/Ekaterina/Documents/scifi1.wav");
    socket = new QTcpSocket(this);

    connect(ui->pbChangePlayer, SIGNAL(clicked()), this, SLOT(pbChangePlayerClicked()));
    connect(ui->pbStatistics, SIGNAL(clicked()), this, SLOT(pbStatisticsClicked()));
    connect(ui->pbRules, SIGNAL(clicked()), this, SLOT(pbRulesClicked()));
    connect(ui->pbNewGame, SIGNAL(clicked()), this, SLOT(pbNewGameClicked()));
    connect(this, SIGNAL(sendUsers(QString)), stat, SLOT(recieveUsers(QString)));

    this->setFixedSize(850, 687);

    for(cCount=0; cCount<colCount; cCount++)
        for(rCount=0; rCount<rowCount; rCount++)
            rects[num++]=QRect(x0+rCount*cellSize, x0+cCount*cellSize, cellSize, cellSize);

    moves=0;
    for(int i=0; i<rowCount; i++)
        for(int j=0; j<colCount; j++)
            arr[i][j]=3;
}

mainwindow::~mainwindow()
{
    delete ui;
}

void mainwindow::pbChangePlayerClicked()
{
    for(int i=0; i<rowCount; i++)
        for(int j=0; j<colCount; j++)
            arr[i][j]=3;
    moves=0;
    repaint();
    this->hide();
    lg->show();
}

void mainwindow::pbStatisticsClicked()
{
    QString users=ui->leUser->text()+":"+ui->leUser2->text()+":";
    emit sendUsers(users);
    this->hide();
    stat->show();
}

void mainwindow::pbRulesClicked()
{
    this->hide();
    rules->show();
}

void mainwindow::pbNewGameClicked()
{
    for(int i=0; i<rowCount; i++)
        for(int j=0; j<colCount; j++)
            arr[i][j]=0;
    moves=0;
    repaint();
    ui->pbNewGame->setEnabled(false);
}

void mainwindow::recieveUser1(QString user)
{
    ui->leUser->setReadOnly(true);
    ui->leUser->setText(user);
}

void mainwindow::recieveUser2(QString user)
{
    ui->leUser2->setReadOnly(true);
    ui->leUser2->setText(user);
}

void mainwindow::paintEvent(QPaintEvent *event)
{
    QPainter *painter=new QPainter(this);
    painter->fillRect(5, 5, 675, 675, QColor("white"));
    painter->setPen(QPen(QBrush(QColor("green")), 5));
    painter->drawRects(rects, rowCount*colCount);
    for(int i=0; i<rowCount; i++)
    {
        for(int u=0; u<colCount; u++)
            if(arr[i][u]==1)
                painter->drawPixmap(rects[i*rowCount+u].x()+10, rects[i*colCount+u].y()+10, QPixmap(":/Doc/x.png"));
            else if(arr[i][u]==2)
                painter->drawPixmap(rects[i*rowCount+u].x()+10, rects[i*colCount+u].y()+10, QPixmap(":/Doc/o.png"));
    }
}

void mainwindow::mousePressEvent(QMouseEvent *event)
{
    for(int i=0; i<rowCount*colCount; i++)
    {
        if(rects[i].contains(event->pos()))
        {
            int row=i/rowCount;
            int col=i%colCount;
            if(arr[row][col]==0)
            {
                click->play();
                moves++;
                arr[row][col]=moves%2==1 ? 1 : 2;
                repaint(rects[i]);
                if(moves==rowCount*colCount)
                {
                    if(check()==1)
                    {
                        repaint();
                        QString text="Игрок "+ui->leUser->text()+" выиграл.";
                        message->play();
                        QMessageBox::information(this, "Конец игры!", text);
                        ui->pbNewGame->setEnabled(true);
                        return;
                    }
                    else if(check()==2)
                    {
                        repaint();
                        QString text="Игрок "+ui->leUser2->text()+" выиграл.";
                        message->play();
                        QMessageBox::information(this, "Конец игры!", text);
                        ui->pbNewGame->setEnabled(true);
                        return;
                    }
                    else if(check()==0)
                    {
                        repaint();
                        QString text="Ничья!";
                        message->play();
                        QMessageBox::information(this, "Конец игры!", text);
                        ui->pbNewGame->setEnabled(true);
                        return;
                    }
                }
            }
            return;
        }
    }
}

int mainwindow::check()
{
    QString user1=ui->leUser->text();
    QString user2=ui->leUser2->text();
    int countX=0, countO=0, x=0, o=0, res;
    int i, j;
    for(i=0; i<rowCount; i++)
    {
        for(j=0; j<colCount; j++)
        {
            if(arr[i][j]==1)
                ++countX;
            else if(arr[i][j]==2)
                ++countO;
        }
        if(countX>countO)
            ++x;
        else
            ++o;
        countX=0;
        countO=0;
    }
    for(i=0; i<rowCount; i++)
    {
        for(j=0; j<colCount; j++)
        {
            if(arr[j][i]==1)
                ++countX;
            else if(arr[j][i]==2)
                ++countO;
        }
        if(countX>countO)
            ++x;
        else
            ++o;
        countX=0;
        countO=0;
    }

    sendStatistics(user1, user2, x, o);

   if(x>o)
       res=1;
   else if(x<o)
       res=2;
   else if(x=o)
       res=0;
    return res;
}

void mainwindow::sendStatistics(QString user1, QString user2, int scoreUser1, int scoreUser2)
{
    QString out="sendstat:"+user1+":"+user2+":"+QString::number(scoreUser1)+":"+QString::number(scoreUser2)+":";

    socket->connectToHost(QHostAddress::LocalHost, 4545);

    if(socket->waitForConnected(5000))
    {
        qDebug() << "Connected!";
        socket->write(out.toLatin1());
        socket->waitForBytesWritten(1000);;
        socket->waitForReadyRead(3000);

        QString cmdFrServ=socket->readAll();
        if(cmdFrServ==QString("fnopened"))
        {
            message->play();
            QMessageBox::information(this, "Ошибка!", "Файл 'statistics.txt' не найден!");
            return;
        }

        socket->close();
    }
    else
    {
        qDebug() << "Not connected!";
    }
}




