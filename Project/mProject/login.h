#ifndef LOGIN_H
#define LOGIN_H

#include <QWidget>
#include "checkin.h"
#include "ui_checkin.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QMessageBox>
#include <QTcpSocket>
#include <QHostAddress>
#include <QDebug>
#include <QtMultimedia/QSound>

namespace Ui {
class LogIn;
}

class mainwindow;
class CheckIn;

class LogIn : public QWidget
{
    Q_OBJECT

public:
    explicit LogIn(QWidget *parent = 0);
    ~LogIn();

private:
    Ui::LogIn *ui;
    mainwindow *mainWindow;
    CheckIn *registrationWindow;
    QSound *message;
    QTcpSocket *socket;

signals:
    void sendUser1(QString user);
    void sendUser2(QString user);

private slots:
    void logInFilled();
    void pbEnterClicked();
    void pbCheckIn1Clicked();
};

#endif // LOGIN_H
